package gug.co.com.testmovies.utils.movies

enum class MoviesFilter {

    POPULAR, // for popular movies

    TOP_RATED, // for top rated movies

    UP_COMING,// for upcoming movies

    GLOBAL // for global search

}