package gug.co.com.testmovies.data.source.remote

enum class NetworkApiStatus {

    // while consult
    LOADING,

    // on error
    ERROR,

    // when is finished
    DONE

}
