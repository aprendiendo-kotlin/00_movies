package gug.co.com.testmovies.data.domain

data class Genre(

    val id: Int,
    val name: String

)