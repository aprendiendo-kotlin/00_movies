package gug.co.com.testmovies.data.domain

data class SpokenLanguage(

    val iso6391: String,
    val name: String

)